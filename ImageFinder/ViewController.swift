//
//  ViewController.swift
//  ImageFinder
//
//  Created by steamulo on 13/02/2019.
//  Copyright © 2019 steamulo. All rights reserved.
//

import UIKit
import Alamofire
import ProgressHUD

class ViewController: UIViewController {
    
    @IBOutlet weak var imageTableView: UITableView!
    // Search view
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    
    let imageCellId = "ImageCellId"
    
    var fakeDataArray = ["Red", "Green", "Blue", "Yellow"]
    var imageArray = Array<Image>()
    
    var imageService = ImageService()
    
    // MARK: - View liefecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Registering image cell
        imageTableView.register(UINib.init(nibName: "ImageCell", bundle: nil), forCellReuseIdentifier: imageCellId)
        
        // Making network request
        findImages()
    }
    
    // MARK: - Privates method
    
    func findImages(withName name: String = "Fleur") {
        imageArray.removeAll()
        self.imageTableView.reloadData()
        
        ProgressHUD.show("Chargement...")
        
        imageService.findImage(name: name) { (images) in
            ProgressHUD.showSuccess()
            
            self.imageArray = images
            self.imageTableView.reloadData()
        }
    }
    
    // MARK: - Actions
    @IBAction func findImagesAction(_ sender: Any) {
        let keyword = self.searchTextField.text!
        
        print(keyword)
        if (keyword.isEmpty) {
            print("Add keyword !!")
            return
        }
        
        // Perform search on keyword
        findImages(withName: keyword)
    }
}

// MARK: - Tableview datasource

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Setup ImageCell
        let imageCell = tableView.dequeueReusableCell(withIdentifier: imageCellId, for: indexPath) as! ImageCell
    
        let imageData = imageArray[indexPath.row]
        imageCell.imageDescription.text = imageData.description
        
        // Loading image
        imageCell.loadImage(imageUrl: imageData.thumbnailUrl)
        
        return imageCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
}

// MARK: - Tableview delegate

extension ViewController: UITableViewDelegate {

}
