//
//  ImageCell.swift
//  ImageFinder
//
//  Created by steamulo on 13/02/2019.
//  Copyright © 2019 steamulo. All rights reserved.
//

import UIKit

class ImageCell: UITableViewCell {
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var imageDescription: UILabel!
    
    func loadImage(imageUrl: String) {
        let imageUrl:URL = URL(string: imageUrl)!
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            let imageData:NSData = NSData(contentsOf: imageUrl)!
            
            // When from background thread, UI needs to be updated on main_queue
            DispatchQueue.main.async {
                let image = UIImage(data: imageData as Data)
                self.cellImageView.image = image
            }
        }
    }
}
